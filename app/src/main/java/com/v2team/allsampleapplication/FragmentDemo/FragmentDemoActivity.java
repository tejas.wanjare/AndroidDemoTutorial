package com.v2team.allsampleapplication.FragmentDemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.v2team.allsampleapplication.R;

public class FragmentDemoActivity extends AppCompatActivity {

    ListView listView;
    String data [] = {"Pune","Mumbai","Nagpur","Nashik","Delhi"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_demo);
        listView = (ListView) findViewById(R.id.listview);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.city_list_item,R.id.textviewlistItem,data);
        listView.setAdapter(arrayAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(FragmentDemoActivity.this, ""+data[position], Toast.LENGTH_SHORT).show();
                switch (position){
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new FirstFragment()).commit();
                        break;

                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SecondFragment()).commit();
                        break;

                        default:
                            break;
                }
            }
        });

    }



}
