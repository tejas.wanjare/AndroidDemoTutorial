package com.v2team.allsampleapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.v2team.allsampleapplication.AndroidMenu.ContextualActionListModeDemoActivity;
import com.v2team.allsampleapplication.AndroidMenu.ContextualActionModeDemoActivity;
import com.v2team.allsampleapplication.AndroidMenu.MenuBarDemoActivity;
import com.v2team.allsampleapplication.AndroidMenu.PopupMenuDemoActivity;
import com.v2team.allsampleapplication.AsyncTaskDemo.AsyncTaskDemoActivity;
import com.v2team.allsampleapplication.FragmentDemo.FragmentDemoActivity;
import com.v2team.allsampleapplication.PermissionsDemo.PermissionsDemoActivity;

public class MainListViewActivity extends AppCompatActivity {

    ListView listView;
    String list [] = {"MenuBarDemo", "ContextualActionMode","ContextualActionListModeDemoActivity","PopupMenuDemo","FragmentDemo","Permission Demo","AsyncTask Demo"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list_view);
        listView = (ListView) findViewById(R.id.listview);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.city_list_item ,R.id.textviewlistItem,list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Intent i ;
                switch (position){
                    case 0 :
                    i = new Intent(MainListViewActivity.this, MenuBarDemoActivity.class);
                        startActivity(i);
                        break;

                    case 1 :
                        i = new Intent(MainListViewActivity.this, ContextualActionModeDemoActivity.class);
                        startActivity(i);
                        break;

                    case 2 :
                        i = new Intent(MainListViewActivity.this, ContextualActionListModeDemoActivity.class);
                        startActivity(i);
                        break;

                    case 3 :
                        i = new Intent(MainListViewActivity.this, PopupMenuDemoActivity.class);
                        startActivity(i);
                        break;

                    case 4 :
                        i = new Intent(MainListViewActivity.this, FragmentDemoActivity.class);
                        startActivity(i);
                        break;

                    case 5 :
                        i = new Intent(MainListViewActivity.this, PermissionsDemoActivity.class);
                        startActivity(i);
                        break;

                    case 6 :
                        i = new Intent(MainListViewActivity.this, AsyncTaskDemoActivity.class);
                        startActivity(i);
                        break;

                    default:
                        break;
                }

            }
        });
    }
}
