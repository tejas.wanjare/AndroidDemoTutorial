package com.v2team.allsampleapplication.PermissionsDemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.v2team.allsampleapplication.R;

public class PermissionsDemoActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    Button check_permission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions_demo);
        check_permission = (Button) findViewById(R.id.check_permission);


    }

    @Override
    protected void onStart() {
        super.onStart();
        check_permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        //not granted hence
                        if (ActivityCompat.shouldShowRequestPermissionRationale(PermissionsDemoActivity.this, Manifest.permission.CAMERA)) {
                            ActivityCompat.requestPermissions(PermissionsDemoActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                        }
                    }
                    else {
                        launchCamera();
                    }

                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCamera();
                } else {
                    Toast.makeText(this, "Permissions denied for camera", Toast.LENGTH_SHORT).show();

                }
        }
    }

    private void launchCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(cameraIntent);
    }


}
