package com.v2team.allsampleapplication.NotificationsDemo;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.v2team.allsampleapplication.MainListViewActivity;
import com.v2team.allsampleapplication.R;

public class NotificationsDemoActivity extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_demo);
        button = (Button) findViewById(R.id.buttonNotification);

    }

    @Override
    protected void onStart() {
        super.onStart();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //create object of NotificationCompat.Builder and set content
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(NotificationsDemoActivity.this, "default");
                        mBuilder.setSmallIcon(R.drawable.ic_launcher_background);
                mBuilder.setContentTitle("My notification");
                mBuilder.setContentText("Much longer text that cannot fit one line...");
                mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
                // code to display longer text applicable on Nougat device
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Much longer text that cannot fit one line...Much longer text that cannot fit one line..."));


                // Create an explicit intent for an Activity in your app
                Intent intent = new Intent(NotificationsDemoActivity.this, MainListViewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(NotificationsDemoActivity.this, 0, intent, 0);
                mBuilder.setContentIntent(pendingIntent);



                //notify notification
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(NotificationsDemoActivity.this);
                // notificationId is a unique int for each notification that you must define
                notificationManager.notify(123, mBuilder.build());

            }
        });
    }
}
