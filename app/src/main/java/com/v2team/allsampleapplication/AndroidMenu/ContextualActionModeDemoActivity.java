package com.v2team.allsampleapplication.AndroidMenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.v2team.allsampleapplication.R;

public class ContextualActionModeDemoActivity extends AppCompatActivity {

    TextView textView;
    ActionMode mActionMode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contextual_action_mode_demo);
        textView = (TextView) findViewById(R.id.textview);
    }

    @Override
    protected void onStart() {
        super.onStart();
        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mActionMode != null){
                    return false;
                }
                mActionMode = startActionMode(callback);
                view.setSelected(true);
                return true;
            }
        });
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.contextualaction,menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            Toast.makeText(ContextualActionModeDemoActivity.this, "onPrepareActionMode", Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.copy :
                    Toast.makeText(ContextualActionModeDemoActivity.this, "copy", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.paste :
                    Toast.makeText(ContextualActionModeDemoActivity.this, "paste", Toast.LENGTH_SHORT).show();
                    break;

            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            Toast.makeText(ContextualActionModeDemoActivity.this, "onDestroyActionMode", Toast.LENGTH_SHORT).show();
            mActionMode = null ;
        }
    };
}
