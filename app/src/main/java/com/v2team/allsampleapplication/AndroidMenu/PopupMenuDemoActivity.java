package com.v2team.allsampleapplication.AndroidMenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.v2team.allsampleapplication.R;

public class PopupMenuDemoActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_menu_demo);
        button = (Button) findViewById(R.id.button);
    }

    @Override
    protected void onStart() {
        super.onStart();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(PopupMenuDemoActivity.this, view);
                popup.setOnMenuItemClickListener(PopupMenuDemoActivity.this);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu, popup.getMenu());
                popup.show();

            }
        });
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.setting:
                Toast.makeText(this, "setting", Toast.LENGTH_SHORT).show();
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                break;

            case R.id.aboutUS:
                Toast.makeText(this, "about us", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }
}
